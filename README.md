# spe-latency.py

Extracts per-load latency information from SPE records in perf.data files.

## EXAMPLE

    # Take SPE sample data
    $ perf record -e arm_spe_0// /bin/some-command
    # Extract per-load latency
    $ spe-latency.py perf.data

## SEE ALSO

[perf-arm-spe(1)](https://man.archlinux.org/man/perf-arm-spe.1.en)
