#!/usr/bin/env python3
#
# Copyright (c) 2022 Ampere Computing LLC.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS
# AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


import bisect
import collections
import ctypes
import enum
import mmap
import os
import struct
import subprocess
import sys

def bits(i):
    n = 0
    while i != 0:
        if i & (1 << n) != 0:
            yield n
        i &= ~(1 << n)
        n += 1

class PerfFileSection(ctypes.Structure):
    @classmethod
    def unpack(cls, buffer, offset=0):
        dest = cls()
        length = len(buffer[offset:offset + ctypes.sizeof(cls)])
        ctypes.memmove(ctypes.addressof(dest), buffer[offset:offset + ctypes.sizeof(cls)], length)
        return dest

    _fields_ = [('offset', ctypes.c_uint64),
                ('size', ctypes.c_uint64)]

class PerfHeader(ctypes.Structure):
    @classmethod
    def unpack(cls, buffer, offset=0):
        dest = cls()
        length = len(buffer[offset:offset + ctypes.sizeof(cls)])
        ctypes.memmove(ctypes.addressof(dest), buffer[offset:offset + ctypes.sizeof(cls)], length)
        i = dest.data.offset + dest.data.size
        dest.sections = {}
        for b in bits(dest.flags):
            e = PerfFileSection.unpack(buffer, i)
            i += ctypes.sizeof(e)
            dest.sections[PerfHeaderFlags.try_into(b)] = e
        return dest

    _fields_ = [('magic', 8 * ctypes.c_char),
                ('size', ctypes.c_uint64),
                ('attr_size', ctypes.c_uint64),
                ('attrs', PerfFileSection),
                ('data', PerfFileSection),
                ('event_types', PerfFileSection),
                ('flags', ctypes.c_uint64),
                ('flags1', 3 * ctypes.c_uint64)]

class PerfHeaderFlags(enum.IntEnum):
    HEADER_TRACING_DATA = 1
    HEADER_BUILD_ID = 2
    HEADER_HOSTNAME = 3
    HEADER_OSRELEASE = 4
    HEADER_VERSION = 5
    HEADER_ARCH = 6
    HEADER_NRCPUS = 7
    HEADER_CPUDESC = 8
    HEADER_CPUID = 9
    HEADER_TOTAL_MEM = 10
    HEADER_CMDLINE = 11
    HEADER_EVENT_DESC = 12
    HEADER_CPU_TOPOLOGY = 13
    HEADER_NUMA_TOPOLOGY = 14
    HEADER_BRANCH_STACK = 15
    HEADER_PMU_MAPPINGS = 16
    HEADER_GROUP_DESC = 17
    HEADER_AUXTRACE = 18
    HEADER_STAT = 19
    HEADER_CACHE = 20
    HEADER_SAMPLE_TIME = 21
    HEADER_SAMPLE_TOPOLOGY = 22
    HEADER_CLOCKID = 23
    HEADER_DIR_FORMAT = 24
    HEADER_BPF_PROG_INFO = 25
    HEADER_BPF_BTF = 26
    HEADER_COMPRESSED = 27
    HEADER_CPU_PMU_CAPS = 28
    HEADER_CLOCK_DATA = 29
    HEADER_HYBRID_TOPOLOGY = 30
    HEADER_HYBRID_CPU_PMU_CAPS = 31

    @classmethod
    def try_into(cls, n):
        try:
            a = cls(n)
            return a
        except:
            return n

class PerfEventType(enum.IntEnum):
    PERF_RECORD_MMAP = 1
    PERF_RECORD_LOST = 2
    PERF_RECORD_COMM = 3
    PERF_RECORD_EXIT = 4
    PERF_RECORD_THROTTLE = 5
    PERF_RECORD_UNTHROTTLE = 6
    PERF_RECORD_FORK = 7
    PERF_RECORD_READ = 8
    PERF_RECORD_SAMPLE = 9
    PERF_RECORD_MMAP2 = 10
    PERF_RECORD_AUX = 11
    PERF_RECORD_ITRACE_START = 12
    PERF_RECORD_LOST_SAMPLES = 13
    PERF_RECORD_SWITCH = 14
    PERF_RECORD_SWITCH_CPU_WIDE = 15
    PERF_RECORD_NAMESPACES = 16
    PERF_RECORD_KSYMBOL = 17
    PERF_RECORD_BPF_EVENT = 18
    PERF_RECORD_CGROUP = 19
    PERF_RECORD_TEXT_POKE = 20
    PERF_RECORD_AUX_OUTPUT_HW_ID = 21
    PERF_RECORD_HEADER_ATTR = 64
    PERF_RECORD_HEADER_EVENT_TYPE = 65
    PERF_RECORD_HEADER_TRACING_DATA = 66
    PERF_RECORD_HEADER_BUILD_ID = 67
    PERF_RECORD_FINISHED_ROUND = 68
    PERF_RECORD_ID_INDEX = 69
    PERF_RECORD_AUXTRACE_INFO = 70
    PERF_RECORD_AUXTRACE = 71
    PERF_RECORD_AUXTRACE_ERROR = 72
    PERF_RECORD_THREAD_MAP = 73
    PERF_RECORD_CPU_MAP = 74
    PERF_RECORD_STAT_CONFIG = 75
    PERF_RECORD_STAT = 76
    PERF_RECORD_STAT_ROUND = 77
    PERF_RECORD_EVENT_UPDATE = 78
    PERF_RECORD_TIME_CONV = 79
    PERF_RECORD_HEADER_FEATURE = 80
    PERF_RECORD_COMPRESSED = 81

    @classmethod
    def try_into(cls, n):
        try:
            a = cls(n)
            return a
        except:
            return n

class PerfEventHeader(ctypes.Structure):
    @classmethod
    def unpack(cls, buffer, offset=0):
        dest = cls()
        length = len(buffer[offset:offset + ctypes.sizeof(cls)])
        ctypes.memmove(ctypes.addressof(dest), buffer[offset:offset + ctypes.sizeof(cls)], length)
        return dest

    _fields_ = [('typ', ctypes.c_uint32),
                ('misc', ctypes.c_uint16),
                ('size', ctypes.c_uint16)]

class AuxtraceEvent(ctypes.Structure):
    @classmethod
    def unpack(cls, buffer, offset=0):
        dest = cls()
        length = len(buffer[offset:offset + ctypes.sizeof(cls)])
        ctypes.memmove(ctypes.addressof(dest), buffer[offset:offset + ctypes.sizeof(cls)], length)
        return dest

    _fields_ = [('header', PerfEventHeader),
                ('size', ctypes.c_uint64),
                ('offset', ctypes.c_uint64),
                ('reference', ctypes.c_uint64),
                ('idx', ctypes.c_uint32),
                ('tid', ctypes.c_uint32),
                ('cpu', ctypes.c_uint32),
                ('reserved__', ctypes.c_uint32)]

class AuxLookup(collections.defaultdict):
    class AuxCpuLookup:
        Entry = collections.namedtuple('AuxCpuLookupEntry', ['key', 'buffer', 'offset'])

        def __init__(self):
            self.a = []

        def __setitem__(self, key, value):
            if not isinstance(key, slice) or key.start is None or key.stop is None or key.step is not None:
                return
            bisect.insort(self.a, self.Entry(key, value[0], value[1]))

        def __getitem__(self, key):
            if not isinstance(key, slice) or key.start is None and key.step is None:
                if isinstance(key, slice):
                    key = key.stop
                i = bisect.bisect(self.a, key, key=lambda e: e.key.start)
                if i < 1:
                    return None
                e = self.a[i - 1]
                if key >= e.key.stop:
                    return None
                return e.buffer[e.offset + key - e.key.start]
            i = bisect.bisect(self.a, key.start or 0, key=lambda e: e.key.start)
            if i < 1:
                return None
            e = self.a[i - 1]
            stop = min(key.stop, e.key.stop)
            if stop <= key.start:
                return None
            rest = None
            if stop < key.stop:
                rest = self[stop:key.stop:key.step]
                if rest is None:
                    return None
            val = e.buffer[e.offset + key.start - e.key.start:e.offset + stop - e.key.start]
            if rest is not None:
                val = val + rest
            if key.step is not None:
                val = val[::key.step]
            return val

    def __init__(self):
        super().__init__(self.AuxCpuLookup)

class SpePackets(collections.defaultdict):
    class SpeCpuPackets:
        def set_buffer(self, buffer):
            self.buffer = buffer
            self.offset = 0

        def __iter__(self):
            return self

        def __next__(self):
            b = self.buffer[self.offset:self.offset + 1]
            self.offset += 1
            if b[0] is None:
                raise StopIteration
            if 0x00 <= b[0] <= 0x1f:
                return b
            if 0x20 <= b[0] <= 0x3f:
               b += self.buffer[self.offset:self.offset + 1]
               self.offset += 1
               length = (1 << ((b[1] >> 4) & 0b11))
               b += self.buffer[self.offset:self.offset + length]
               self.offset += length
            if 0x40 <= b[0] <= 0x4f or \
               0x80 <= b[0] <= 0x8f or \
               0xc0 <= b[0] <= 0xcf:
               b += self.buffer[self.offset:self.offset + 1]
               self.offset += 1
               return b
            if 0x50 <= b[0] <= 0x5f or \
               0x90 <= b[0] <= 0x9f or \
               0xd0 <= b[0] <= 0xdf:
               b += self.buffer[self.offset:self.offset + 2]
               self.offset += 2
               return b
            if 0x60 <= b[0] <= 0x6f or \
               0xa0 <= b[0] <= 0xaf or \
               0xe0 <= b[0] <= 0xef:
               b += self.buffer[self.offset:self.offset + 4]
               self.offset += 4
               return b
            if 0x70 <= b[0] <= 0x7f or \
               0xb0 <= b[0] <= 0xbf or \
               0xf0 <= b[0] <= 0xff:
               b += self.buffer[self.offset:self.offset + 8]
               self.offset += 8
               return b

    def __init__(self):
        super().__init__(self.SpeCpuPackets)


def spe_event(packets):
    out = []
    for p in packets:
        if len(p) == 1 and p[0] == 0:
            continue
        out.append(p)
        if p[0] == 0x01 or p[0] == 0x71:
            return out
    return None

def fix_va(a):
    o = a & ((1 << 52) - 1)
    if a & (1 << 52) == 0:
        return o
    else:
        return o | (0b11111111_1111 << 52)

InstructionVirtualAddress = collections.namedtuple('InstructionVirtualAddress', ['address', 'el', 'ns'])
def spe_pc(event):
    for packet in event:
        if len(packet) == 9 and packet[0] == 0xb0 or len(packet) == 10 and packet[0] == 0x20 and packet[1] == 0xb0:
            v = struct.unpack('Q', packet[-8:])[0]
            return InstructionVirtualAddress(fix_va(v & 0xffffffffffffff), (v >> 61) & 0b11, v >> 63)
    return None

DataVirtualAddress = collections.namedtuple('DataVirtualAddress', ['address', 'tag'])
def spe_data_va(event):
    for packet in event:
        if len(packet) == 9 and packet[0] == 0xb2 or len(packet) == 10 and packet[0] == 0x20 and packet[1] == 0xb2:
            v = struct.unpack('Q', packet[-8:])[0]
            return DataVirtualAddress(fix_va(v & 0xffffffffffffff), (v >> 56))
    return None

DataPhysicalAddress = collections.namedtuple('DataPhysicalAddress', ['address', 'pat', 'ch', 'ns'])
def spe_data_pa(event):
    for packet in event:
        if len(packet) == 9 and packet[0] == 0xb3 or len(packet) == 10 and packet[0] == 0x20 and packet[1] == 0xb3:
            v = struct.unpack('Q', packet[-8:])[0]
            return DataPhysicalAddress(v & 0xffffffffffffff, (v >> 56) & 0b1111, (v >> 62) & 0b1, (v >> 63))
    return None

class SpeCounterType(enum.IntEnum):
    TOTAL = 0
    ISSUE = 1
    TRANSLATION = 2

def spe_latency(event, index):
    for packet in event:
        if len(packet) == 3 and packet[0] == (0x98 + index) or len(packet) == 4 and packet[0] == 0x20 and packet[1] == (0x98 + index):
            v = struct.unpack('H', packet[-2:])[0]
            return v & 0xfff
    return None

def spe_is_load(event):
    for packet in event:
        if packet[0] != 0x49:
            continue
        return packet[1] & 1 == 0

def spe_is_l1d_refill(event):
    for packet in event:
        if (packet[0] & 0xcf) != 0x42:
            continue
        return (packet[1] & 0x8) != 0
    return False

if 'PERF_EXEC_PATH' in os.environ:
    sys.path.append(os.environ['PERF_EXEC_PATH'] +
                    '/scripts/python/Perf-Trace-Util/lib/Perf/Trace')

    from perf_trace_context import *
    from Core import *

def trace_begin():
    global aux, packets
    fd = os.open(os.environ['PERF_FILE'], os.O_RDONLY)
    dat = mmap.mmap(fd, os.fstat(fd).st_size, prot=mmap.PROT_READ)
    p = PerfHeader.unpack(dat)
    aux = AuxLookup()
    i = p.data.offset
    while i < p.data.offset + p.data.size:
        e = PerfEventHeader.unpack(dat, i)
        if e.typ == PerfEventType.PERF_RECORD_AUXTRACE:
            i += e.size
            e = AuxtraceEvent.unpack(dat, i - e.size)
            aux[e.cpu][e.offset:e.offset + e.size] = (dat, i)
        i += e.size
    packets = SpePackets()
    for cpu in aux:
        packets[cpu].set_buffer(aux[cpu])
    print('comm,dso,symbol,issue,translation,execute')

def process_event(event):
    p = spe_event(packets[event['sample']['cpu']])
    if not spe_is_load(p) or not spe_is_l1d_refill(p):
        return
    pc = f'{event["symbol"]}+{event["symoff"]:#x}' if 'symbol' in event else hex(event['sample']['ip'])
    print(event['comm'], event['dso'], pc,
          spe_latency(p, SpeCounterType.ISSUE),
          spe_latency(p, SpeCounterType.TRANSLATION),
          spe_latency(p, SpeCounterType.TOTAL) -
          spe_latency(p, SpeCounterType.TRANSLATION) -
          spe_latency(p, SpeCounterType.ISSUE),
          sep=',')

if __name__ == '__main__' and 'PERF_EXEC_PATH' not in os.environ:
    path = sys.argv[1] if len(sys.argv) >= 2 else 'perf.data'
    os.environ['PERF_FILE'] = path
    os.close(2)
    os.execvp('perf', ['perf', 'script', '-i', path, '--itrace=i1i', __file__])
    sys.exit(main(*sys.argv[1:]))
